#include "Arduino.h"
#ifndef THRESHOLDSENSOR_H_
#define THRESHOLDSENSOR_H_

/**
 * remember to call begin() in setup and loop() in loop
 */
class ThresholdSensor
{
private:
    uint8_t pinSensor;
    unsigned long lastChange = 0;
    uint16_t threshold = 1000;
    boolean active = false;
    boolean previousValue = false;
    boolean highIsActive = true;

public:
    ThresholdSensor(const uint8_t, const uint8_t, boolean );
    void setThreshold(uint16_t ms);
    void begin();
    void loop();
    boolean isActive();
};

#endif /* THRESHOLDSENSOR_H_ */
